//
//  ListViewController.swift
//  SwiftWebinar
//
//  Created by max on 29/06/16.
//  Copyright © 2016 Max. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {
    
    var resourceName: String!
    var tableName: String!
    var key: String!
    
    var rows: [HHFWManagedObject]?

    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let moc = HHFWManagedObjectContext(name: resourceName, setURL: "", decryptKey: key)
        let request = HHFWFetchRequest(entityName: tableName)
        rows = moc!.executeFetchRequest(request!)
    }
    
    @IBAction func logoutTouched(sender: AnyObject) {
        let alert = UIAlertController(title: "Logout", message: "Are you sure?", preferredStyle: .Alert)
        let ok = UIAlertAction(title: "Logout", style: .Destructive, handler: {(action: UIAlertAction) in
            HHFWController.sharedInstance().logout(nil)
            self.dismissViewControllerAnimated(true, completion: nil)
        })
        let cancel = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        alert.addAction(ok)
        alert.addAction(cancel)
        self.presentViewController(alert, animated: true, completion: nil)
    }
}

extension ListViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .Default, reuseIdentifier: "cell")
        cell.textLabel?.text = rows![indexPath.row].toStringWithSeparator("_")
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if rows != nil {
            return rows!.count
        } else {
            return 0
        }
    }
    
}

//
//  ViewController.swift
//  SwiftWebinar
//
//  Created by max on 27/06/16.
//  Copyright © 2016 Max. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let url = "http://suzuki.sp.saprun.com/sync/v0.4/env_dev/proj_demo/app_demo/"
    let resourceName = "ZSR_MP_12M_GET_500"
    let tableName = "ZSR_MP_12M_GET_500_$_ET_ROWS"
//        let resourceName = "GETMATERIALS"
//        let tableName = "GETMATERIALS_$_GETMATERIALS"

    var key: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func loginButtonTouched(sender: AnyObject) {
        let user = userName.text
        let pwd = password.text
        key = user!.stringByAppendingString(pwd!)
        if user != nil && pwd != nil {
            HHFWController.sharedInstance().authLogin(user!, password: pwd!, url: url, deviceID: "123456", completionHandler: {(result: [String]?, curlCode: Int, httpCode: Int) in
                if (curlCode != 0 || httpCode != 0) {
                    if curlCode == 7 || curlCode == 6 {
                        self.offline()
                    }
                    // Optional
                    else if httpCode == 401 {
                        let alert = UIAlertController(title: "Error", message: "No access", preferredStyle: .Alert)
                        let ok = UIAlertAction(title: "Close", style: .Cancel, handler: nil)
                        alert.addAction(ok)
                        dispatch_async(dispatch_get_main_queue(), {() in
                            self.presentViewController(alert, animated: true, completion: nil)})
                    }
                    //
                    else {
                        print("Something wrong, curlCode: ", curlCode, " httpCode: ", httpCode);
                    }
                } else {
                    self.online()
                }
            })
        }
    }

    @IBAction func clearCacheButtonTouched(sender: AnyObject) {
        HHFWController.sharedInstance().dropCache()
    }
    
    // Offline
    func offline() {
        let alert = UIAlertController(title: "Offline mode", message: "Using local database", preferredStyle: .Alert)
        let ok = UIAlertAction(title: "OK", style: .Cancel, handler: {(action: UIAlertAction) in
            self.performSegueWithIdentifier("list", sender: self)})
        alert.addAction(ok)
        dispatch_async(dispatch_get_main_queue(), {() in
            self.presentViewController(alert, animated: true, completion: nil)
        })
    }
    
    // Online
    func online() {
        dispatch_async(dispatch_get_main_queue(), {() in
            self.activityIndicator.startAnimating()
        })
        HHFWController.sharedInstance().setKey(key)
        HHFWController.sharedInstance().getResourceDeltaFromURL(url, resourceName: resourceName, delegate: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "list" {
            if let listVC = segue.destinationViewController as? ListViewController {
                listVC.key = self.key
                listVC.resourceName = resourceName
                listVC.tableName = tableName
            }
        }
    }
}

extension ViewController: HHFWDelegate {
    func getResourceDeltaCompleted(operationID: UInt, result: String?) {
        print(result)
        dispatch_async(dispatch_get_main_queue(), {() in
            self.activityIndicator.stopAnimating()
            self.performSegueWithIdentifier("list", sender: self)
        })
    }
}


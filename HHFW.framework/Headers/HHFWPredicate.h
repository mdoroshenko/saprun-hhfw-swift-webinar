//
//  HHFWPredicate.h
//  HHFW
//
//  Created by max on 25/04/16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HHFWPredicate : NSObject

// Predicate format, for request representation
@property(readonly, nonatomic, nonnull, strong) NSString *predicateFormat;

// Instantiate predicate with format
+ (instancetype _Nonnull)predicateWithFormat:(NSString * _Nonnull)format;

@end

//
//  CppDelegate.h
//  ios-native
//
//  Created by Максим Дорошенко on 29.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//
//  Main CppRun wrapper

#import <Foundation/Foundation.h>
#import "HHFWDelegate.h"

@interface HHFWController : NSObject

+ (instancetype _Nonnull)sharedInstance;

#pragma mark - Main methods
// Authorization
// DOS if httpCode && curlCode is 999
- (void)authLogin:(NSString * _Nonnull)login
         password:(NSString * _Nonnull)password
              url:(NSString * _Nonnull)url
         deviceID:(NSString * _Nonnull)deviceID
completionHandler:(void(^ _Nullable)(NSArray<NSString *> * _Nullable availableResources, NSInteger curlErrorCode, NSInteger httpErrorCode))handler;

// Logout
- (void)logout:(_Nullable id<HHFWDelegate>)delegate;

// Get resource delta
- (NSInteger)getResourceDeltaFromURL:(NSString * _Nonnull)sBaseURL
                        resourceName:(NSString * _Nonnull)sResourceName
                            delegate:(_Nullable id<HHFWDelegate>)delegate;

// POST request
- (NSInteger)postWithURL:(NSString * _Nonnull)sURL
                delegate:(_Nullable id<HHFWDelegate>)delegate;
// Add parameter to POST request
- (void)addPostParameterWithKey:(NSString * _Nonnull)sKey
                          value:(NSString * _Nonnull)sValue
                       delegate:(_Nullable id<HHFWDelegate>)delegate;

// GET request
- (NSInteger)getWithBaseURL:(NSString * _Nonnull)sURL
                   delegate:(_Nullable id<HHFWDelegate>)delegate;

// Abort operation
- (void)abortOperation:(unsigned int)operationID
              delegate:(_Nullable id<HHFWDelegate>)delegate;

// Subscribe for topic, MQTT
- (NSInteger)subscribeToServer:(NSString * _Nonnull)zAddr
                      forTopic:(NSString * _Nonnull)topic
                   mqttVersion:(int)mqttVersion
                  cleanSession:(BOOL)cleanSession
                      delegate:(_Nullable id<HHFWDelegate>)delegate;

// SSL checks
- (void)setSLLChecks:(BOOL)bEnabled;

#pragma mark - Other tools

// ReadCookie
- (NSInteger)readCookie:(_Nullable id<HHFWDelegate>)delegate;

// Set log level
- (void)setLogLevel:(int)nLogLevel
           delegate:(_Nullable id<HHFWDelegate>)delegate;

// Set sqlite crypt key
- (void)setKey:(NSString * _Nullable)key;

// sqlite3_exec
- (NSSet<NSDictionary *> * _Nullable)sqlite3_exec:(NSString * _Nonnull)zFileName
            zRequest:(NSString * _Nonnull)zRequest;

// Remove all sqlite files
- (void)dropCache;

@end

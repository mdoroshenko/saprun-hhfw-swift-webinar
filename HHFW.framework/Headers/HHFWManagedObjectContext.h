//
//  HHFWManagedObjectContext.h
//  HHFW
//
//  Created by max on 25/04/16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HHFWFetchRequest.h"
#import "HHFWManagedObject.h"
#import "HHFWController.h"

@class HHFWEntityDescription;

@interface HHFWManagedObjectContext : NSObject<HHFWDelegate>

// Name of context(Resource)
@property (nonatomic, strong, nonnull) NSString *name;

#pragma mark - Context methods

// Init with name
- (instancetype _Nullable)initWithName:(NSString * _Nonnull)name setURL:(NSString * _Nonnull)setURL decryptKey:(NSString * _Nullable)key;

// Execute fetch request
- (NSArray <HHFWManagedObject *> * _Nonnull)executeFetchRequest:(HHFWFetchRequest * _Nonnull)request;

// Save context with completion block
- (void)saveEntity:(NSString * _Nonnull)entityName completion:(void(^_Nullable)(NSInteger, NSString* _Nullable))completion;

#pragma mark - Object methods
// Create object
- (HHFWManagedObject * _Nullable)createObjectWithEntityName:(NSString * _Nonnull)entityName;

// Commit object to local storage
- (void)commitObject:(HHFWManagedObject * _Nullable)managedObject withEntityName:(NSString * _Nonnull)entityName;

// TODO: Unused, remove

// Force push object to remote server
- (void)pushObject:(HHFWEntityDescription * _Nonnull)entityDescription;

// Remove object from local storage
- (void)removeObject:(HHFWEntityDescription * _Nonnull)entityDescription;

@end

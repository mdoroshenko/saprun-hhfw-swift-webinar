//
//  HHFWManagedObject.h
//  HHFW
//
//  Created by max on 25/04/16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HHFWManagedObject : NSObject

@property (nullable, nonatomic, strong) NSMutableDictionary *attributes;

@property (nullable, nonatomic, strong) NSMutableSet<NSString *> *ignoreKeys;

// Init entity with members dictionary
- (instancetype _Nullable)initWithAttributes:(NSDictionary * _Nullable)attributes;

// Apply ignore list, remove abuse keys
- (void)applyIgnoreList;

// JSON representation of attributes
- (NSData * _Nullable)jsonValue;

- (NSData * _Nullable)jsonValueDataOnly;

// Returns self.attributes as string
- (NSString * _Nullable)toStringWithSeparator:(NSString * _Nonnull)separator;

@end

//
//  CppWrapperDelegate.h
//  ios-native
//
//  Created by Максим Дорошенко on 29.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//
//  Simple delegate for C++ -> ObjC callbacks

#import <Foundation/Foundation.h>

@protocol HHFWDelegate <NSObject>

@optional

// ReadCookie completed
- (void)readCookieCompleted:(NSUInteger)operationID result:(NSString * _Nullable)result;

// Get resource delta completed
- (void)getResourceDeltaCompleted:(NSUInteger)operationID result:(NSString * _Nullable)result;
// GET completed
- (void)getCompleted:(NSUInteger)operationID result:(NSString * _Nullable)result;

// POST completed
- (void)postCompleted:(NSUInteger)operationID result:(NSString * _Nullable)result;
// Add post parameter completed
- (void)addPostParameterCompleted:(NSUInteger)operationID result:(NSString * _Nullable)result;



// Abort completed
- (void)abortCompleted:(NSUInteger)operationID result:(NSString * _Nullable)result;

// Set log level completed
- (void)setLogLevelCompleted:(NSUInteger)operationID result:(NSString * _Nullable)result;

// Subscribe for topic completed
- (void)subscribeForTopicCompleted:(NSUInteger)operationID result:(NSString * _Nullable)result;

// Logout completed
- (void)logoutCompleted:(NSUInteger)operationID result:(NSString * _Nullable)result;

@end

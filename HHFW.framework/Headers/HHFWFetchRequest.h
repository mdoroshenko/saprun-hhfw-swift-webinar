//
//  HHFWFetchRequest.h
//  HHFW
//
//  Created by max on 25/04/16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HHFWPredicate.h"

@interface HHFWFetchRequest : NSObject

// Predicate
@property (nonatomic, nonnull, strong) HHFWPredicate *predicate;

// Entity name
@property (nonatomic, nonnull, strong, readonly) NSString *entityName;

// Fetch request with entity name
+ (instancetype _Nullable)fetchRequestWithEntityName:(NSString * _Nonnull)entityName;

@end

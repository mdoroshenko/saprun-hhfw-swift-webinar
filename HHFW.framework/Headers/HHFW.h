//
//  HHFW.h
//  HHFW
//
//  Created by max on 15/04/16.
//  Copyright © 2016 EigenMethod. All rights reserved.
//
//  HyperHive framework

#import <HHFW/HHFWController.h>
#import <HHFW/HHFWDelegate.h>
#import <HHFW/HHFWManagedObjectContext.h>
#import <HHFW/HHFWEntityDescription.h>
#import <HHFW/HHFWManagedObject.h>
#import <HHFW/HHFWFetchRequest.h>
#import <HHFW/HHFWPredicate.h>
#import <HHFW/HHFWDelegate.h>

//! Project version number for HHFW.
FOUNDATION_EXPORT double HHFWVersionNumber;

//! Project version string for HHFW.
FOUNDATION_EXPORT const unsigned char HHFWVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <HHFW/PublicHeader.h>



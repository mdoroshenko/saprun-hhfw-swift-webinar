//
//  HHFWEntityDescription.h
//  HHFW
//
//  Created by max on 25/04/16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HHFWManagedObjectContext, HHFWManagedObject;

@interface HHFWEntityDescription : NSObject

@property (nonatomic, strong, nonnull) NSString *name;

//+ (HHFWManagedObject * _Nullable)insertNewObjectForEntityForName:(NSString * _Nonnull)entityName inManagedObjectContext:(HHFWManagedObjectContext * _Nonnull)context;

@end
